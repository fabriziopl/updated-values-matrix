/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
public class Library {
	
	public String[][] updatedValues(String oldValue, String newValue, String[][] value)  {
	   for (int k = 0; k < value.length; k++) {
		   for(int i=0;i<value[k].length;i++) {
			   if (oldValue.equals(value[k][i])) {
				   value[k][i] = newValue;
			   }
		   }
	   }
	   
	   return value;
	}
		
}
