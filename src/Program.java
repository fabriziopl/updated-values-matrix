import javax.swing.JFrame;
import javax.swing.WindowConstants;

import executerpane.LibraryExecuterPanel;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */
public class Program extends JFrame {
	private static final long serialVersionUID = 1583724102189855698L;

	public Program(Class<?> libraryClass) {
		super();
		setSize(650, 450);
		LibraryExecuterPanel libraryExecuterPanel = new LibraryExecuterPanel(libraryClass);
		getContentPane().add(libraryExecuterPanel);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		validate();
		setResizable(true);
		setVisible(true);
		libraryExecuterPanel.requestFocus();
	}
	
	public static void main(String[] args) {
		new Program(Library.class);
	}
}
