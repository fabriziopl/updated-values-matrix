import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es) 
 * @version 1.0
 */
public class LibraryTest {

	@Before
	public void setUp() throws Exception {

	}
		
	@Test
	public void testExercise_A() {
	    // Given
	    Library library = new Library();
	    // When
	   String [] [] value = new String [][] {{"Juan"}};
	   String [] [] updatedValue = library.updatedValues("Rosa", "Ana", value);
	   // Then
		assertArrayEquals(new String[][]{{"Juan"}},  updatedValue);
	}
	
	@Test
	public void testExercise_B() {
	    // Given
		Library library = new Library();
	    // When
	   String [] [] value = new String [][] {{"Rosa"}};
	   String [] [] updatedValue = library.updatedValues("Rosa", "Ana", value);
	   // Then
		assertArrayEquals(new String[][]{{"Ana"}},  updatedValue);
	}
	
	@Test
	public void testExercise_C() {
	    // Given
		Library library = new Library();
	    // When
	   String [] [] value = new String [][] {{"Pepe","Juan"}};
	   String [] [] updatedValue = library.updatedValues("Juan", "Ana", value);
	   // Then
		assertArrayEquals(new String[][]{{"Pepe","Ana"}},  updatedValue);
	}
	
	@Test
	public void testExercise_D() {
	    // Given
		Library library = new Library();
	    // When
	   String [] [] value = new String [][] {{"Pepe"},{"Juan"}};
	   String [] [] updatedValue = library.updatedValues("Juan", "Ana", value);
	   // Then
		assertArrayEquals(new String[][]{{"Pepe"},{"Ana"}},  updatedValue);
	}
	
	@Test
	public void testExercise_E() {
	    // Given
		Library library = new Library();
	    // When
	   String [] [] value = new String [][] {{"Luis","Rosa"},{"Luis","Luis"},{"Juan","Luis"}};
	   String [] [] updatedValue = library.updatedValues("Luis", "Pepe", value);
	   // Then
		assertArrayEquals(new String[][]{{"Pepe","Rosa"},{"Pepe","Pepe"},{"Juan","Pepe"}},  updatedValue);
	}

}

