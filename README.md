Created by Francisco Guerra (francisco.guerra@ulpgc.es) for teaching.

Realizar los siguientes ejercicios para conseguir el 50% de la nota:

1. Programar el paso ROJO del test testExercise_A que comprueba que el método updatedValues(String,String,String[][]):String[][] devuelve la matriz [["Juan"]] cuando recibe en el primer parámetro el String "Rosa", en el segundo parámetro el String "Ana" y en el tercer parámetro la matriz [["Juan"]]

2. Programar el paso VERDE del test testExercise_A

3. Programar el paso ROJO del test testExercise_B que comprueba que el método updatedValues(String,String,String[][]):String[][] devuelve la matriz [["Ana"]] cuando recibe en el primer parámetro el String "Rosa", en el segundo parámetro el String "Ana" y en el tercer parámetro la matriz [["Rosa"]]

4. Programar el paso VERDE del test testExercise_B y además el test anterior sigue siendo VERDE

5. Programar el paso ROJO del test testExercise_C que comprueba que el método updatedValues(String,String,String[][]):String[][] devuelve la matriz [["Pepe","Ana"]] cuando recibe en el primer parámetro el String "Juan", en el segundo parámetro el String "Ana" y en el tercer parámetro la matriz [["Pepe","Juan"]]

6. Programar el paso VERDE del test testExercise_C y además los tests anteriores siguen siendo VERDE

7. Programar el paso ROJO del test testExercise_D que comprueba que el método updatedValues(String,String,String[][]):String[][] devuelve la matriz [["Pepe"],["Ana"]] cuando recibe en el primer parámetro el String "Juan", en el segundo parámetro el String "Ana" y en el tercer parámetro la matriz [["Pepe"],["Juan"]]

8. Programar el paso VERDE del test testExercise_D y además los tests anteriores siguen siendo VERDE

9. Programar el paso ROJO del test testExercise_E que comprueba que el método updatedValues(String,String,String[][]):String[][] devuelve la matriz [["Pepe","Rosa"],["Pepe","Pepe"],["Juan","Pepe"}]] cuando recibe en el primer parámetro el String "Luis", en el segundo parámetro el String "Pepe" y en el tercer parámetro la matriz [["Luis","Rosa"],["Luis","Luis"],["Juan","Luis"]]

10. Programar el paso VERDE del test testExercise_E y además los tests anteriores siguen siendo VERDE

Programar el método updatedValues(String,String,String[][]):String[][] para que funcione para cualquier caso y conseguir el otro 50% de la nota:  Es decir, dado una String que se pasa como primer parámetro, otra String que se pasa como segundo parámetro y una matriz de Strings que se pasa como tercer parámetro, debe devolver una matriz con las Strings del tercer parámetro, cambiando cada String que sea igual a la String del primer parámetro por la String del segundo parámetro.